# `autowrap_c`: automatically generated wrappers for C

Provides a procedural macro `#[_c]` that generates a wrapper for a function.  The wrapper function:

- packs raw C datatypes into rust composite datatype inputs (tuples, slices, [`Option`]s), and
- unpacks composite rust datatype outputs (same as above, plus [`Result`]s).

Hopefully this reduces the boilerplate required to write a rust library that maintains an
`extern "C"` interface.

[`Option`]: https://doc.rust-lang.org/std/option/enum.Option.html
[`Result`]: https://doc.rust-lang.org/std/result/enum.Result.html
