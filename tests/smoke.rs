use autowrap_c::_c;
use num_complex::{Complex32, Complex64};
use num_integer::Integer;
use num_traits::cast::NumCast;
use num_traits::Num;
use num_traits::{Float, Signed, Unsigned};
use std::default::Default;
use std::fmt::Debug;
use std::result::Result;

#[allow(non_camel_case_types)]
pub type c32 = Complex32;
#[allow(non_camel_case_types)]
pub type c64 = Complex64;
#[allow(non_camel_case_types)]
pub type r32 = f32;
#[allow(non_camel_case_types)]
pub type r64 = f64;

#[derive(Debug)]
#[repr(transparent)]
pub struct Status(i32);

impl Default for Status {
    fn default() -> Self {
        Status(0)
    }
}

pub trait UInt: Num + Integer + NumCast + Unsigned {}
impl<T> UInt for T where T: Num + Integer + NumCast + Unsigned {}
pub trait Int: Num + Integer + NumCast + Signed {}
impl<T> Int for T where T: Num + Integer + NumCast + Signed {}
pub trait Real: Num + Float + NumCast {}
impl<T> Real for T where T: Num + Float + NumCast {}
pub trait Scalar: Num + NumCast {}
impl Scalar for r32 {}
impl Scalar for r64 {}
impl Scalar for c32 {}
impl Scalar for c64 {}

#[allow(dead_code)]
fn cast<F: NumCast, T: NumCast>(f: F) -> Result<T, Status> {
    <T as NumCast>::from(f).ok_or(Status(-1))
}

#[allow(unused_parens)]
#[_c]
pub fn foo_1(x: (u32, u32), z: &[u32; 3]) -> (i32) {
    (x.0 * z[0] + x.1 * z[1]) as i32 - (z[2] as i32)
}

#[test]
fn anonymous_tuple() {
    let x = (5u32, 6u32);
    let z = [7u32, 8u32, 9u32];
    assert_eq!(foo_1(x, &z), foo_1_c(x.0, x.1, &z));
}

#[_c]
/// foo_2 documentation
pub fn foo_2((x, y): (u32, u32), z: r64) -> (i32,) {
    (f64::floor(x as f64 * z + y as f64 * z) as i32,)
}

#[test]
fn destructured_tuple() {
    let x = 5u32;
    let y = 6u32;
    let z = 7.0f64;
    assert_eq!(foo_2((x, y), z).0, foo_2_c(x, y, z));
}

#[_c]
pub fn foo_3(a: &[i32], _b: ()) -> &[i32] {
    let n = a.len() / 2;
    &a[0..n]
}

#[test]
fn slices() {
    let a = &[0, 1, 1, 2, 3, 5, 8, 13, 21, 34];
    let inptr: *const i32 = a.as_ptr();
    let mut outptr: *const i32 = std::ptr::null();
    let outlen = unsafe { foo_3_c(inptr, a.len(), &mut outptr) };
    let rustout = foo_3(a, ());
    let rustoutptr: *const i32 = rustout.as_ptr();
    assert_eq!(rustoutptr, outptr);
    assert_eq!(rustout.len(), outlen);
}

#[_c]
pub fn foo_4(a: &mut [i32], b: (i32,)) -> ((i32, i32), i32) {
    (
        (
            *a.iter().min().unwrap_or(&-1),
            *a.iter().max().unwrap_or(&-2),
        ),
        b.0 + *a.iter().min().unwrap_or(&-3),
    )
}

#[test]
fn output_tuple() {
    let a = &mut [0, 1, 1, 2, 3, 5, 8, 13, 21, 34];
    let b = (-10,);
    let mut cout1: i32 = i32::MAX;
    let mut cout2: i32 = i32::MAX;
    let inptr: *mut i32 = a.as_mut_ptr();
    let cout3 = unsafe { foo_4_c(inptr, a.len(), b.0, &mut cout1, &mut cout2) };
    assert_eq!(foo_4(a, b), ((cout1, cout2), cout3))
}

#[_c]
pub fn foo_5(_a: Option<i32>) -> () {}

#[_c]
pub fn foo_5_2(#[_c(null)] _a: Option<i32>) -> Option<i32> {
    None
}

#[_c]
pub fn foo_5_3(#[_c(-1)] _a: Option<i32>) -> () {}

#[_c]
pub fn foo_6(_a: Option<&i32>) -> Option<Option<i32>> {
    None
}

#[_c]
pub fn foo_7(_a: Option<&mut i32>) -> Result<u32, Status> {
    Err(Status(-1))
}

#[_c]
pub fn foo_8(_a: Option<&[i32]>) -> Result<Option<f32>, Status> {
    Err(Status(-1))
}

pub mod custom_result {
    use super::Status;
    use autowrap_c::_c;
    use std::result::Result as StdResult;

    type Result<T> = StdResult<T, Status>;

    #[_c(Status)]
    pub fn foo_9(_a: Option<&mut [i32]>, _b: (Option<u32>, Option<&f64>)) -> Result<u32> {
        Err(Status(-1))
    }

    #[_c(Status)]
    pub fn foo_10(_a: Option<(i32, (f64, u32))>) -> Result<Option<&'static [i32]>> {
        Err(Status(-1))
    }
}

#[_c]
pub fn foo_11<'a, I: Int, U: UInt, R: Real, S: Scalar>(
    _a: &I,
    _b: U,
    _c: R,
    _d: S,
) -> Result<&'a I, Status> {
    Err(Status(-1))
}
